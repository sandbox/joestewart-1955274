@hosting-servers
Feature: Aegir Hosing Server page
  In order to have a view of the aegir server administration page
  As an admin
  I should go to aegir hosting server administration page

  Background:
    Given I am on the homepage

  @anon @wip
  Scenario: Confirm I cannot view the Servers as an anonymous user
    And I am not logged in
    Then I should see the text "Access denied"

  @anon @wip
  Scenario: Confirm I see the login form
    And I am not logged in
    Then I should see the text "User login"
     And I should see the link "Request new password"

  @admin
  Scenario: Confirm I am viewing the frontpage as an admin
    And I am logged in as "admin"
    And I follow "Servers"
    Then I should see the text "Add server"

