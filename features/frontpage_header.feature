@front @specific_text
Feature: View home page header
  In order to have access to different sections
  As a logged in user
  I need to be able to view navigations links

  @user
  Scenario Outline: View bottom header tabs as authenticated user
    Given I am logged in as "admin"
    And I am on "<page>"
    And I should see the following <links> in "navigation" area
    | links     |
    | Sites     |
    | Servers   |
    | Platforms |

    Examples:
    | page |
    | /    |
