@hosting-platforms
Feature: Aegir Hosing Platforms page
  In order to have a view of the aegir platforms administration page
  As an admin
  I should go to aegir hosting server administration page

  Background:
    Given I am on the homepage

  Scenario: Confirm I am viewing the platforms as an admin
    And I am logged in as "admin"
    And I follow "Platforms"
    Then I should see the text "Add platform"

