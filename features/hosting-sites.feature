@hosting-sites
Feature: Aegir Hosing Sites page
  In order to have a view of the aegir sites administration page
  As an admin
  I should go to aegir hosting sites administration page

  Background:
    Given I am on the homepage

  Scenario: Confirm I am viewing the sites as an admin
    And I am logged in as "admin"
    And I follow "Sites"
    Then I should see the text "Add site"

