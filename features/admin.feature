@admin
Feature: admin role abilities
In order to effectively use the site
  As a site admin
  I should be able to view the list, modify and add content

  Background:
    Given I am logged in as "admin"

  @admin-content
  Scenario Outline: Create page is available to a site admin
    When I visit "<page>"
    Then I should see "<text>"
    Examples:
      | page               | text            |
      | /node/add/client   | Create Client   |
      | /node/add/platform | Create Platform |
      | /node/add/server   | Create Server   |
      | /node/add/site     | Create Site     |

  @admin-content
  Scenario Outline: Managing content is available to a site admin
    When I visit "<page>"
    Then I should see "<text>"
    Examples:
      | page                | text                  |
      | /admin/content/node | Show only items where |

  @admin-hosting
  Scenario Outline: Managing hosting is available to a site admin
    When I visit "<page>"
    Then I should see the following <links>
      | links                      |
      | Features                   |
      | Clients                    |
      | Platform pathauto settings |
      | Queues                     |
      | Settings                   |

    Examples:
      | page           |
      | /admin/hosting |


  @admin-hosting
  Scenario Outline: Managing hosting is available to a site admin
    When I visit "<page>"
    Then I should see "<text>"
    Examples:
      | page                             | text                         |
      | /admin/hosting/features          | Optional system features:    |
      | /admin/hosting/client            | Client internal name prefix: |
      | /admin/hosting/platform_pathauto | Base path:                   |
      | /admin/hosting/queues            | Frequency                    |
      | /admin/hosting/settings          | General Hosting settings:    |

