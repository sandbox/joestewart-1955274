@hosting-tasks
Feature: Aegir Hosing Tasks page
  In order to have a view of the aegir tasks administration page
  As an admin
  I should go to aegir hosting tasks administration page

  Background:
    Given I am on the homepage

  Scenario: Confirm I am viewing the sites as an admin
    And I am logged in as "admin"
    And I follow "More tasks"
    Then I should see the text "Task queue"

